import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import moment from 'moment';
import { Subscription } from 'rxjs';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit,OnDestroy {
  subscription: Subscription[] = [];

  public gradientStroke;
  public chartColor;
  public canvas : any;
  public ctx;
  public gradientFill;
  public lineBigDashboardChartOptions:any;

  public gradientChartOptionsConfiguration: any;
  public gradientChartOptionsConfigurationWithNumbersAndGrid: any;

  public lineChartType;
  public lineChartData:Array<any>;
  public lineChartOptions:any;
  public lineChartLabels:Array<any>;
  public lineChartColors:Array<any>

  public lineChartWithNumbersAndGridType;
  public lineChartWithNumbersAndGridData:Array<any>;
  public lineChartWithNumbersAndGridOptions:any;
  public lineChartWithNumbersAndGridLabels:Array<any>;
  public lineChartWithNumbersAndGridColors:Array<any>

  public lineChartGradientsNumbersType;
  public lineChartGradientsNumbersData:Array<any>;
  public lineChartGradientsNumbersOptions:any;
  public lineChartGradientsNumbersLabels:Array<any>;
  public lineChartGradientsNumbersColors:Array<any>

  public hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
      g = parseInt(hex.slice(3, 5), 16),
      b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
      return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
      return "rgb(" + r + ", " + g + ", " + b + ")";
    }
  }
  stats:any=[]
  constructor(private router:Router,private clientService:ClientService) {
    this.clientService.getAdminDetails()
    this.getChildrens();
   }

  ngOnInit() {
    var dt = new Date()
var currentWeekDay = dt.getDay();
var lessDays = currentWeekDay == 0 ? 4 : currentWeekDay-1
var wkStart =new Date(new Date(dt).setDate(dt.getDate()- lessDays));
var wkEnd =moment( new Date(new Date(wkStart).setDate(wkStart.getDate()+4))).format('YYYY-MM-DD');
console.log("start end 123",wkStart,wkEnd)

    this.chartColor = "#FFFFFF";

    this.lineChartLabels = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    this.lineChartOptions = this.gradientChartOptionsConfiguration;

    this.lineChartType = 'line';

    this.canvas = document.getElementById("lineChartExampleWithNumbersAndGrid");


      this.lineChartWithNumbersAndGridColors = [
       {
         borderColor: "#18ce0f",
         pointBorderColor: "#FFF",
         pointBackgroundColor: "#18ce0f",
         backgroundColor: this.gradientFill
       }
     ];
    this.lineChartWithNumbersAndGridLabels = ["12pm,", "3pm", "6pm", "9pm", "12am", "3am", "6am", "9am"];
    this.lineChartWithNumbersAndGridOptions = this.gradientChartOptionsConfigurationWithNumbersAndGrid;

    this.lineChartWithNumbersAndGridType = 'line';

    this.canvas = document.getElementById("barChartSimpleGradientsNumbers");

    this.lineChartGradientsNumbersType = 'bar';
  }
totalChildrens:number=0;
totalClients:any=0
  getChildrens=()=>{
    this.subscription.push(
this.clientService.getChildrens().subscribe((resp:any)=>{
  this.totalChildrens=resp?.data.length
  console.log("childrens",this.totalChildrens)
this.getClientsCount()
})
    )
  }
  getClientsCount(){
    this.subscription.push(
    this.clientService.getClientsCount().subscribe((resp:any)=>{
      this.totalClients=resp.data;
      console.log("my count data",this.totalClients)
      this.stats=[
      {title:'TOTAL DE CLIENTES',value:this.totalClients?.totalClient,icon:'account_circle',color:'bg-danger', url:'client-details',filter:'all'},
      {title:'CLIENTES ACTIVOS',value:this.totalClients?.activeClients,icon:'star',color:'bg-success',url:'billing-detail',filter:'active'},
      {title:'CLIENTES INACTIVOS',value:this.totalClients?.inactiveClients,icon:'notifications_off',color:'bg-primary',url:'billing-detail',filter:'inactive'},
      {title:'ARCHIVADO',value:this.totalClients?.archiveClients,icon:'warning',color:'bg-warning',url:'client-details',filter:'archive'},
      {title:'DASARCHIVAR',value:this.totalClients?.unArchiveClients,icon:'sentiment_satisfied_alt',color:'bg-primary',url:'client-details',filter:'unArchive'},

      {title:'TOTAL DE ALUMNOS',value:this.totalChildrens,icon:'account_box',color:'bg-danger',url:'/dashboard',filter:''}
    ]
    })
    )
  }

  filterClientDetailsData(url,data){
    this.router.navigate([url], { state:  {filter: data,} });
      }
  ngOnDestroy(): void {
    this.subscription.forEach(s => s.unsubscribe());
  }
}
