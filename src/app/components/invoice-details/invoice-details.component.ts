import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
// import { NgxPrintService } from 'ngx-print';
@Component({
  selector: "app-invoice-details",
  templateUrl: "./invoice-details.component.html",
  styleUrls: ["./invoice-details.component.scss"],
})
export class InvoiceDetailsComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<InvoiceDetailsComponent>,
    // @Inject(String) private printService: NgxPrintService

  ) {}

  weeksDataSource = this.data?.collectPaymentWeeks;
  clientDetails=this.data?.clientDetails
  ngOnInit(): void {
  }
  onClose() {
    this.dialogRef.close();
  }
  collectPayment() {
    const invoiceData={
      reasonForExtraCost:this.data?.reasonForExtraCost,
      paymentMode:this.data?.paymentMode,
      extraCost:this.data?.extraCost,
      discountCost:this.data?.discountCost,
      collectPaymentWeeks:this.data?.collectPaymentWeeks,
      clientId:this.data?.clientId,
      totalPayment:this.data?.totalPayment,
      subTotal:this.data?.subTotal
    }
    this.dialogRef.close(invoiceData);
  }
  printInvoice(){
    console.log("print invlice clicked")
    // window.print();
    // this.printService.print(['ngxPrint'], true);
  }
}
