import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
declare var window: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: "/dashboard",
   title: "Dashboard",
   icon: "design_app",
   class: "" },
  {
    path: "/add-client",
    title: "AGREGAR CLIENTE",
    icon: "ui-1_simple-add",
    class: "",
  },
  {
    path: "/client-details",
    title: "DETALLES DE CLIENTE",
    icon: "users_single-02",
    class: "",
  },
  {
    path: "/billing-detail",
    title: "Detalles de facturación",
    icon: "business_bank",
    class: "",
  },
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  formModal: any;

  constructor(private router: Router) {}

  ngOnInit() {
    this.menuItems = ROUTES.filter((menuItem) => menuItem);
    this.formModal = new window.bootstrap.Modal(
      document.getElementById("logoutModal"),
      { keyboard: false, backdrop: "static" }
    );
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
  openModal() {
    this.formModal.show();
  }
  closeModal() {
    this.formModal.hide();
  }
  LoggedOut() {
    localStorage.removeItem("token");
    this.router.navigate(["/login"]);
  }
}
