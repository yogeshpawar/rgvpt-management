import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from "@angular/material/dialog";
import { Router } from '@angular/router';
import moment from "moment";
import { Subscription } from "rxjs";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { AdminService } from "../..//services/admin.service";
import { CandidateService } from "../../services/candidate.service";
import { ClientService } from "../../services/client.service";
import { StatusUpdateComponent } from "../status-update/status-update.component";
@Component({
  selector: "app-add-client",
  templateUrl: "./add-client.component.html",
  styleUrls: ["./add-client.component.scss"],
})
export class AddClientComponent implements OnInit, AfterViewInit, OnDestroy {
  form: FormGroup;
  childrensLength: number = 0;
  subscription: Subscription[] = [];

  CostRates: any = {};
  today = moment(new Date()).format("YYYY-MM-DD");
  isEditable: boolean = true;
  costTypes = [];
  disableSubmit=false
  adminDetails: any;
  schoolsList:any=[]
  currentUser: any;
  Amount = 0;
  constructor(
    private fb: FormBuilder,
    private candidateService: CandidateService,
    private router: Router,
    private dialog:MatDialog,
    private adminService: AdminService,
    private clientService: ClientService
  ) {
    this.adminDetails = JSON.parse(localStorage.getItem("userDetails"));
    this.createControls();
    this.addNewChild();

    this.currentUser =
      this.router.getCurrentNavigation()?.extras?.state?.currentUser;
    if (this.currentUser != undefined) {
      this.isEditable =
        this.router.getCurrentNavigation()?.extras?.state?.action == "edit"
          ? true
          : false;
    }
  }
  ngAfterViewInit(): void {
    if (this.currentUser != undefined) {
      for (let i = 0; i < this.currentUser.childrens.length - 1; i++) {
        this.addNewChild();
      }
      console.log("current user", this.currentUser);
      this.form.patchValue({ ...this.currentUser });
      this.Amount = this.currentUser.totalAmount;
    }
  }
  getCostData() {
    this.subscription.push(
      this.adminService.getCost(this.adminDetails._id).subscribe(
        (result: any) => {
          //  this.CostRates=result.data[0];
          for (let data of result.data) {
            this.costTypes.push(data.costType);
            this.CostRates = {
              ...this.CostRates,
              [data.costType]: data.costRate,
            };
          }
        },
        (error) => {
          console.log("error", error.message);
        }
      )
    );
  }
  getSchoolsData() {
    this.adminService.getSchools(this.adminDetails._id).subscribe(
      (result: any) => {
        this.schoolsList=result.data
      },
      (error) => {
        console.log("error", error.message);
      }
    );
  }
  UpdateClientData() {
    const childData = [];
    for (let i = 0; i < this.childrensLength; i++) {
      childData.push({
        ...this.currentUser.childrens[i],
        ...this.form.value.childrens[i],
      });
    }
    this.clientService
      .updateClientData(this.currentUser._id, {
        ...this.form.value,
        childrens: childData,
      })
      .subscribe(
        (result) => {
          this.router.navigate(["./client-details"]);
          Swal.fire({
            icon: "success",
            title: "Congratulation ...",
            text: "Client Updated Successfully",
            timer: 3000,
          });
        },
        (error) => {
          Swal.fire({
            icon: "error",
            title: `Oops...${error.error.status} `,
            text: error.error.message,
            timer: 3000,
          });
        }
      );
  }
  createControls() {
    this.form = this.fb.group({
      clientId: [
        Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000,
        [Validators.required, Validators.minLength(6), Validators.maxLength(6)],
      ],
      adminId: [this.adminDetails._id],
      fullName: ["", [Validators.required]],
      primaryPhoneNumber: [
        "",
        [
          Validators.required,
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      secondaryPhoneNumber: [
        "",
        [
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      relationship: ["", [Validators.required]],
      childrens: this.fb.array([]),
    });
  }

  conformDeleteChild(childIndex){
    const dialogRef = this.dialog.open(StatusUpdateComponent, {
      data: {name:this.form.value.childrens[childIndex].childName, dataFor: "deleteChild" },
      disableClose: true,
      width: "70vw",
      maxWidth: "550px",
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == "yes") {
this.deleteChild(childIndex)
      }
    })
  }
  selectedChildCostType() {
    this.Amount = 0;
    this.form.value.childrens.map((curr, index) => {
      // (this.form.get("childrens") as FormArray)
      //   .at(index)
      //   .get("payableAmount")
      //   .patchValue(this.CostRates[curr.costType]);
      this.Amount += parseInt(this.CostRates[curr.costType]) ?? 0;
      this.candidateService.paymentAmount.next(this.Amount);
    });
    this.form.patchValue({ totalAmount: this.Amount });
  }

  addNewChild() {
    this.childrensLength += 1;
    this.childrens().push(this.newChild());
  }

  childrens(): FormArray {
    return this.form.get("childrens") as FormArray;
  }

  newChild(): FormGroup {
    return this.fb.group({
      childName: ["", [Validators.required]],
      dateOfBirth: ["", [Validators.required]],
      schoolName: ["", [Validators.required]],
      costType: ["", [Validators.required]],
      // payableAmount: [null],
    });
  }
  deleteChild(index: number) {
    if (this.currentUser && this.currentUser?.childrens[index] !== undefined) {
      this.currentUser.childrens = this.currentUser?.childrens.map(
        (curr, ind) => {
          if (index == ind) {
            return { _id: curr._id, delete: true };
          } else {
            return curr;
          }
        }
      );
    }
    const add = this.form.get("childrens") as FormArray;
    add.removeAt(index);
    // this.childrensLength -= 1;
    this.selectedChildCostType();
  }

  submitForm() {
    this.disableSubmit=true
    this.subscription.push(
      this.clientService.postClient(this.form.value).subscribe(
        res => {
          if (res) {
            Swal.fire({
              icon: "success",
              title: "Congratulation ...",
              text: "New Client Added Successfully",
              timer: 3000,
            });
            this.disableSubmit=false
            this.router.navigate(["./client-details"]);
          }
        },
        error => {
          this.disableSubmit=false;
        }

      )
    );
  }

  ngOnInit(): void {
    this.getCostData();
    this.getSchoolsData()
  }

  hasError(fieldName, validationName) {
    return (
      this.form.get(fieldName)?.touched &&
      this.form.get(fieldName)?.hasError(validationName)
    );
  }
  hasErrorChild(fieldName, validationName, index) {
    return (
      this.childrens().controls[index].get(fieldName).touched &&
      this.childrens().controls[index].get(fieldName).hasError(validationName)
    );
  }
  ngOnDestroy(): void {
    this.subscription.forEach((s) => s.unsubscribe());
  }
}
