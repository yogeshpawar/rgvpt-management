import { Component, EventEmitter, Input, Output } from "@angular/core";
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from "@angular/material/datepicker";
import moment from "moment";
import { ToastrService } from "ngx-toastr";
import { DatepickerService } from "../../services/datepicker.service";
@Component({
  selector: "app-datepicker",
  templateUrl: "./datepicker.component.html",
  styleUrls: ["./datepicker.component.scss"],
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: DatepickerService,
    },
  ],
})
export class DatepickerComponent {
  @Input() start: Date;
  @Input() end: Date;

  @Output() selectedData: EventEmitter<any> = new EventEmitter();
  fromDate;
  toDate;
  tempArray = [];
  week: any = {};
  todayDate: Date = new Date();
  dateFilterFn = (date: Date) => {
    return ![0].includes(date.getDay()) && ![6].includes(date.getDay());
  };
  constructor(private toasterService: ToastrService) {}
  selectedStartDate(event: any) {
    this.fromDate = event;
  }

  selectedEndDate(event: any) {
    this.toDate = event;
    if (
      this.fromDate == "Invalid date" ||
      this.toDate == "Invalid date" ||
      this.fromDate == null ||
      this.toDate == null
    ) {
      this.toasterService.error(
        "Please Select Week from previous month",
        "Error",
        {
          timeOut: 3500,
          closeButton: true,
          toastClass: "alert alert-error alert-with-icon",
          positionClass: "toast-top-center",
        }
      );
    } else {
      this.selectedData.emit({
        fromDate: moment(this.fromDate).format("YYYY-MM-DD"),
        toDate: moment(this.toDate).format("YYYY-MM-DD"),
      });
      this.tempArray.push({ fromDate: this.fromDate, toDate: this.toDate });
    }
  }
}
