import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { AuthService } from "../../services/auth.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthService
  ) {}
  form: FormGroup;
  subscription: Subscription[] = [];
  ngOnInit(): void {
    this.createControls();
  }

  createControls() {
    this.form = this.formBuilder.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });
  }

  loginForm() {
    if (this.form.value.email && this.form.value.password) {
      this.auth.adminLogin(this.form.value).subscribe(
        (result: any) => {
          if (result && result.token) {
            Swal.fire({
              icon: "success",
              title: "Welcome",
              text: "Your Login Successfully",
              timer: 3000,
            });
            localStorage.setItem("token", `${result.token}`);
            localStorage.setItem("user", "admin");
            localStorage.setItem("userDetails", JSON.stringify(result.data));
            this.router.navigate(["/dashboard"]);
            this.form.patchValue({ password: "" });
          }
        },
        (error) => {
          Swal.fire({
            icon: "error",
            title: `Oops...`,
            text: error.error.message,
            timer: 3000,
          });
          this.form.reset();
        }
      );
    }
  }
  hasError(fieldName, validationName) {
    return (
      this.form.get(fieldName)?.touched &&
      this.form.get(fieldName)?.hasError(validationName)
    );
  }

  forgotPassword() {
    this.router.navigate(["/forgot-password"]);
  }

  ngOnDestroy(): void {
    this.subscription.forEach((s) => s.unsubscribe());
  }
}
