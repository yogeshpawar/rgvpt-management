import {
  Component,
  Inject,
  OnInit
} from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { ClientService } from '../../services/client.service';

@Component({
  selector: "app-status-update",
  templateUrl: "./status-update.component.html",
  styleUrls: ["./status-update.component.scss"],

})
export class StatusUpdateComponent implements OnInit {
  weeklyPayment: any = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private clientService:ClientService,
    public dialogRef: MatDialogRef<StatusUpdateComponent>
  ) {}


  displayedChildColumns: string[] = [
    "position",
    "childName",
    "dateOfBirth",
    "schoolName",
    "costType",
    "PayableAmount"
  ];
  childDataSource = this.data?.childrens;

  ngOnInit(): void {}

  deleteChild(){
    this.dialogRef.close('yes');
  }
  deleteSchool(){
    this.dialogRef.close('yes');
}
  changeStatus(data){
    this.dialogRef.close(data);
  }

  onClose() {
    this.dialogRef.close();
  }
}
