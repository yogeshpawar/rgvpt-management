import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxPrintModule } from 'ngx-print';
import { ToastrModule } from 'ngx-toastr';
import { AlphanumericDirective } from '../directives/alphanumeric.directive';
import { NumbersOnlyDirective } from '../directives/numbers-only.directive';
import { AddClientComponent } from './add-client/add-client.component';
import { BillingDetailComponent } from './billing-detail/billing-detail.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { FooterComponent } from './footer/footer.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PaymentCollectComponent } from './payment-collect/payment-collect.component';
import { SettingsComponent } from './settings/settings.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { StatusUpdateComponent } from './status-update/status-update.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatTableModule,
    MatSlideToggleModule,
    MatDividerModule,
    NgxPrintModule,
    MatIconModule,
    ToastrModule,
    MatCardModule,
    NgxPaginationModule,
    MatSnackBarModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    MatDialogModule,
    MatRippleModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    FormsModule,
    NgbModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    AlphanumericDirective,
    NumbersOnlyDirective,
    LoginComponent,
    BillingDetailComponent,
    AddClientComponent,
    ForgotPasswordComponent,
    SettingsComponent,
    ClientDetailsComponent,
    DatepickerComponent,
    StatusUpdateComponent,
    PaymentCollectComponent,
    InvoiceDetailsComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent
  ]
})
export class ComponentsModule { }
