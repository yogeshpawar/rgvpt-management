import { CurrencyPipe } from "@angular/common";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from "@angular/material/datepicker";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import moment from "moment";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { AdminService } from "../..//services/admin.service";
import { discountRate, END, paymentMode, START } from "../../mockData";
import { ClientService } from "../../services/client.service";
import { DatepickerService } from "../../services/datepicker.service";
import { InvoiceDetailsComponent } from "../invoice-details/invoice-details.component";

@Component({
  selector: "app-payment-collect",
  templateUrl: "./payment-collect.component.html",
  styleUrls: ["./payment-collect.component.scss"],
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: DatepickerService,
    },
    CurrencyPipe,
  ],
})
export class PaymentCollectComponent implements OnInit, OnDestroy {
  clientDetails: any;
  form: FormGroup;
  childDataSource: any = [];
  weeksDataSource: any = [];
  discountRate = discountRate;
  collectPaymentWeeks: any = [];
  childrensTotal: number;
  selectedDiscount: number = 0;
  extraAmount: number = 0;
  totalPayment: number = 0;
  fromDate: any;
  moment: any = moment;
  toDate: any;
  lastWeek: Date;
  subTotal: number = 0;
  paymentMode = paymentMode;
  todayDate: Date = new Date();
  start = START;
  subscription: Subscription[] = [];
  recentPaidWeek: any = {};
  weeklyPayment: any = [];
  end = END;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private clientService: ClientService,
    public dialog: MatDialog,
    public adminService:AdminService,
    private toasterService: ToastrService,
    private currency: CurrencyPipe
  ) {
    this.clientDetails =
      this.router.getCurrentNavigation()?.extras?.state?.data;
    this.createControl();
    console.log("client details123",this.clientDetails)
  }
  displayedChildColumns: string[] = [
    "position",
    "childName",
    "schoolName",
    "PayableAmount",
  ];
  totals: any = {};
  selectDiscount() {
    this.selectedDiscount = parseInt(this.form.value?.discountCost);
    this.totalPayment =
      this.subTotal + this.extraAmount - this.selectedDiscount;
  }
  getWeeklyPayment() {
    this.clientService.getWeeklyPayment(this.clientDetails?._id).subscribe(
      (result: any) => {
        for (let data of result[0]?.weeklyPayment) {
          this.weeklyPayment.push({ ...data });
        }
      },
      (error) => {
        Swal.fire({
          icon: "error",
          title: `Oops...${error.error?.status} `,
          text: error.error?.message,
          timer: 3000,
        });
      }
    );
  }
  isWeekSelected() {
    let flag = true;
    for (let [index, week] of this.weeklyPayment.entries()) {
      if (
        (moment(week.fromDate).format("YYYY-MM-DD") == this.fromDate ||
          moment(week?.toDate).format("YYYY-MM-DD") == this.toDate) &&
        week?.paymentStatus == "active"
      ) {
        flag = false;
        break;
      }
      if (
        (moment(week.fromDate).format("YYYY-MM-DD") == this.fromDate ||
          moment(week?.toDate).format("YYYY-MM-DD") == this.toDate) &&
        week?.paymentStatus != "active"
      ) {
        this.weeklyPayment.splice(index, 1);
      }
    }
    if (flag) {
      this.collectPaymentWeeks.push({
        fromDate: this.fromDate,
        toDate: this.toDate,
        paidAmount: this.clientDetails?.totalAmount,
        paymentStatus: "active",
      });

      this.weeklyPayment.push({
        fromDate: this.fromDate,
        toDate: this.toDate,
        paidAmount: this.clientDetails?.totalAmount,
        paymentStatus: "active",
      });

      this.subTotal =
        this.clientDetails?.totalAmount * this.collectPaymentWeeks.length ?? 0;
      this.totalPayment =
        this.subTotal + this.extraAmount - this.selectedDiscount;

      this.weeksDataSource = new MatTableDataSource(this.collectPaymentWeeks);
    } else {
      this.displayNotificationMessage(
        "This Week is Already Selected",
        "error",
        "Error"
      );
    }
  }
  collectPayment() {
    if (this.collectPaymentWeeks.length > 0) {
      const dialogRef = this.dialog.open(InvoiceDetailsComponent, {
        data: {
          ...this.form.value,
          clientId: this.clientDetails?._id,
          clientDetails:this.clientDetails,
          subTotal: this.subTotal,
          totalPayment: this.totalPayment,
          discountCost: this.form.value.discountCost??0,
          collectPaymentWeeks: this.collectPaymentWeeks,
        },
        disableClose: true,
        hasBackdrop: true,
        width: "60vw",
        maxWidth: "600px",
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result != undefined) {
          let data = { invoiceData: result, weeklyPayment: this.weeklyPayment };
          this.changedStatus(data);
        }
      });
    } else {
      this.displayNotificationMessage(
        "Please select al least one week",
        "error",
        "Error"
      );
    }
  }

  changedStatus(value) {
    this.subscription.push(
      this.clientService
        .updateClientStatus(this.clientDetails, value)
        .subscribe(
          (res) => {
            if (res) {
              this.router.navigate(["/billing-detail"]);
              console.log("after update ==>", res);
              Swal.fire({
                icon: "success",
                title: "Congrats ...",
                text: "Payment Paid Successfully",
                timer: 3000,
              });
            }
          },
          (error) => {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: error.error.message,
              timer: 3000,
            });
          }
        )
    );
  }
  deleteWeek(index) {
    this.collectPaymentWeeks.splice(index, 1);
    this.subTotal =
      this.clientDetails?.totalAmount * this.collectPaymentWeeks.length ?? 0;
    this.totalPayment =
      this.subTotal + this.extraAmount - this.selectedDiscount;
    this.weeksDataSource = new MatTableDataSource(this.collectPaymentWeeks);
  }
  getTotal() {
    this.childrensTotal =this.clientDetails?.totalAmount
    //  this.clientDetails?.childrens.reduce(
    //   (accum, curr) => accum + curr?.payableAmount,
    //   0
    // );
    this.subTotal = this.childrensTotal;
    this.totalPayment = this.subTotal;
  }
  selectedStartDate(event: any) {
    this.fromDate = moment(event).format("YYYY-MM-DD");
  }

  selectedEndDate(event: any) {
    this.toDate = moment(event).format("YYYY-MM-DD");
    console.log("selected week ", this.fromDate, this.toDate);
    if (
      this.fromDate == "Invalid date" ||
      this.toDate == "Invalid date" ||
      this.fromDate == null ||
      this.toDate == null
    ) {
      this.displayNotificationMessage(
        "Please Select Week from previous month",
        "error",
        "Error"
      );
      this.fromDate = "";
      this.toDate = "";
    } else {
      this.isWeekSelected();
    }
  }
  dateFilterFn = (date: Date) => {
    return ![0].includes(date.getDay()) && ![6].includes(date.getDay());
  };

  ngOnInit(): void {
    this.getWeeklyPayment();
    // this.getCostData()
    console.log("inside on init",this.clientDetails?.childrens)
    this.childDataSource = new MatTableDataSource(
      this.clientDetails?.childrens
    );
    this.getTotal();
    if (this.clientDetails?._id) {
      this.clientService
        .getLatestInvoiceWeek(this.clientDetails?._id)
        .subscribe((resp: any) => {
          let maxWeek = resp[0];
          for (let i = 0; i < resp.length; i++) {
            if (
              moment(resp[i]?.collectPaymentWeeks?.fromDate).format(
                "YYYY-MM-DD"
              ) >
              moment(maxWeek?.collectPaymentWeeks?.fromDate).format(
                "YYYY-MM-DD"
              )
            ) {
              maxWeek = resp[i];
            }
          }
          this.recentPaidWeek = maxWeek;
          if (this.recentPaidWeek != undefined) {
            this.lastWeek = new Date(
              moment(this.recentPaidWeek?.collectPaymentWeeks?.toDate).format(
                "YYYY-MM-DD"
              )
            );
            let mydate = this.lastWeek.setDate(this.lastWeek.getDate() + 1);
          } else {
            this.lastWeek = new Date(this.start);
            let mydate = this.lastWeek.setDate(this.lastWeek.getDate() - 1);
          }
        });
    }
  }
  createControl() {
    this.form = this.fb.group({
      paymentMode: ["",[Validators.required]],
      discountCost: [""],
      extraCost: [null],
      reasonForExtraCost: [""],
    });
  }

  selectedExtraCost() {
    this.extraAmount = this.form.value.extraCost;
    this.totalPayment =
      this.subTotal + this.extraAmount - this.selectedDiscount;
  }

  hasError(fieldName, validationName) {
    return (
      this.form.get(fieldName)?.touched &&
      this.form.get(fieldName)?.hasError(validationName)
    );
  }

  displayNotificationMessage(message, type, heading) {
    if (type == "error") {
      this.toasterService.error(` ${message}`, heading, {
        timeOut: 3500,
        closeButton: true,
        toastClass: "alert alert-error alert-with-icon",
        positionClass: "toast-top-center",
      });
    } else {
      this.toasterService.success(` ${message}`, heading, {
        timeOut: 3500,
        positionClass: "toast-top-center",
        toastClass: "alert alert-success alert-with-icon",
      });
    }
  }

  ngOnDestroy(): void {
    this.subscription.forEach((s) => s.unsubscribe());
  }
}
