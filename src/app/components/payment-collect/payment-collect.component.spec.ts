import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCollectComponent } from './payment-collect.component';

describe('PaymentCollectComponent', () => {
  let component: PaymentCollectComponent;
  let fixture: ComponentFixture<PaymentCollectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentCollectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCollectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
