import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import * as moment from "moment";
import { Subject, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { END, START } from "../../mockData";
import { CandidateService } from "../../services/candidate.service";
import { ClientService } from "../../services/client.service";

@Component({
  selector: "app-billing-detail",
  templateUrl: "./billing-detail.component.html",
  styleUrls: ["./billing-detail.component.scss"],
})
export class BillingDetailComponent implements OnInit, OnDestroy {
  moment: any = moment;
  clientsList: any[] = [];
  subscription: Subscription[] = [];
  page: number = 1;
  start = START;
  end = END;
  totalClients: number;
  statusFilter = "all";
  today = new Date().toLocaleDateString();
  private debounceSubject = new Subject<string>();

  currentCustomer: any;
  constructor(
    private router: Router,
    private candidateService: CandidateService,
    private clientService: ClientService,
    public dialog: MatDialog
  ) {
    if (this.router.getCurrentNavigation()?.extras?.state?.filter) {
      this.statusFilter =
        this.router.getCurrentNavigation()?.extras?.state?.filter;
      this.selectedStatus(this.statusFilter);
    }
    this.getAllClients();
  }

  ngOnInit(): void {}
  filename = `student Reports ${new Date().toLocaleString()}`;

  selectedStatus(status) {
    this.page = 1;
    this.getAllClients();
  }
  searchValue=''
  fromDate: any;
  toDate: any;
  getAllClients() {
    const status = this.statusFilter == "all" ? "undefined" : this.statusFilter;
    this.fromDate = moment(this.start).format("YYYY-MM-DD");
    this.toDate = moment(this.end).format("YYYY-MM-DD");
    this.subscription.push(
      this.clientService
        .getClients(this.fromDate, this.toDate, status, this.page,this.searchValue)
        .subscribe((result: any) => {
          this.totalClients = result.total;
          this.clientsList = result.data;
        })
    );
  }
  openStatusDialog(data) {
    this.currentCustomer = data;
    this.router.navigate(["payment"], { state: { data: data } });
  }
  renderPage(event: number) {
    this.page = event;
    this.getAllClients();
  }
  getSearchValue(value){

    this.page = 1;
    if(value!="" && value!=null){
      this.searchValue=value
      this.debounceSubject.next(value);
      this.debounceSubject.pipe(
        debounceTime(2000),
        distinctUntilChanged(),
      ).subscribe((value) => {
        this.getAllClients()
      });
    }else {
      this.searchValue=""
      this.getAllClients()
    }
  }

  changedStatus(value) {
    this.subscription.push(
      this.clientService
        .updateClientStatus(this.currentCustomer, value)
        .subscribe(
          (res) => {
            if (res) {
              this.getAllClients();
              console.log(res);
              Swal.fire({
                icon: "success",
                title: "Congrats ...",
                text: "Payment Status Updated Successfully",
                timer: 3000,
              });
            }
          },
          (error) => {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: error.error.message,
              timer: 3000,
            });
          }
        )
    );
  }

  selectedWeek(data) {
    this.start = data.fromDate;
    this.end = data.toDate;
    this.getAllClients();
  }

  exportClients() {
    let fromDate = moment(this.start).format("YYYY-MM-DD");
    let toDate = moment(this.end).format("YYYY-MM-DD");
    this.subscription.push(
      this.clientService
        .exportClient(fromDate, toDate, this.statusFilter,this.searchValue)
        .subscribe((res) => {
          console.log("resuit", res);
          Swal.fire({
            icon: "success",
            title: "Congrats ...",
            text: "File Exported Successfully",
            timer: 2000,
          });
        })
    );
  }

  ngOnDestroy(): void {
    this.subscription.forEach((s) => s.unsubscribe());
  }
}
