import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { AuthService } from "../../services/auth.service";
import { CandidateService } from "../../services/candidate.service";
declare var window: any;
@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"],
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  formModal: any;
  formModal2: any;
  subscription: Subscription[] = [];

  constructor(
    private candidateService: CandidateService,
    private formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthService
  ) {}
  form: FormGroup;
  resetPasswordForm: FormGroup;

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
    });

    this.formModal = new window.bootstrap.Modal(
      document.getElementById("myModal"),
      { backdrop: "static", keyboard: false }
    );
    this.formModal2 = new window.bootstrap.Modal(
      document.getElementById("myModal2"),
      { backdrop: "static", keyboard: false }
    );
    this.formModal.show();
    this.setControlsResetPasswordForm();
  }

  setControlsResetPasswordForm() {
    this.resetPasswordForm = this.formBuilder.group(
      {
        password: ["", [Validators.required]],
        confirmPassword: ["", [Validators.required]],
      },
      { validator: this.passwordMatchValidator }
    );
  }
  passwordMatchValidator(frm: FormGroup) {
    return frm.controls["password"].value ===
      frm.controls["confirmPassword"].value
      ? null
      : { mismatch: true };
  }
  resetPassword() {
    this.subscription.push(
      this.auth
        .resetAdminPassword({
          ...this.resetPasswordForm.value,
          ...this.form.value,
        })
        .subscribe(
          (res) => {
            if (res) {
              Swal.fire({
                icon: "success",
                title: "Congrats ...",
                text: "Password Updated Successfully",
                timer: 3000,
              });
              this.formModal2.hide();
              this.resetPasswordForm.reset();
              this.form.reset();
              this.router.navigate(["./login"]);
            }
          },
          (error) => {
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: error.error.message,
              timer: 3000,
            });
          }
        )
    );
  }
  hasPasswordError(fieldName, validationName) {
    return (
      this.resetPasswordForm.get(fieldName)?.touched &&
      this.resetPasswordForm.get(fieldName)?.hasError(validationName)
    );
  }

  hasError(fieldName, validationName) {
    return (
      this.form.get(fieldName)?.touched &&
      this.form.get(fieldName)?.hasError(validationName)
    );
  }

  submitEmail() {
    this.subscription.push(
      this.auth.verifyEmailAddress(this.form.value).subscribe(
        (resp) => {
          if (resp) {
            this.formModal.hide();
            this.formModal2.show();
          }
        },
        (error) => {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Your Email Address is Not Matched ! Please Enter correct Email",
            timer: 3000,
          });
        }
      )
    );
  }
  ngOnDestroy(): void {
    this.subscription.forEach((s) => s.unsubscribe());
    this.formModal2.hide();
    this.formModal.hide();
  }
}
