import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { AdminService } from "../..//services/admin.service";
import { StatusUpdateComponent } from "../status-update/status-update.component";

declare var window: any;
@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent implements OnInit {
  displayedColumns: string[] = [
    "position",
    "amountType",
    "amount",
    "updateCost",
  ];
  displayedSchoolColumns: string[] = [
    "position",
    "schoolName",
    "updateSchool",
    "deleteSchool"
  ];

  dataSource: any = [];
  schoolsDataSource:any=[]
  formModal: any;
  currentCost: any = {};
  currentSchool: any = {};
  form: FormGroup;
  schoolForm:FormGroup;
  CostRates: any = {};
  editValue: boolean = false;
  aboutSchoolDialogBox=false;
  schoolEditValue:boolean=false
  adminDetails:any
  // costValue:any;
  fieldName: any;
  constructor(private adminService: AdminService, private fb: FormBuilder, private dialog:MatDialog,) {
    this.adminDetails = JSON.parse(localStorage.getItem("userDetails"));
    this.getCostData();
    this.schoolFormController()
  }

  ngOnInit(): void {
    this.getSchoolsData()
    this.formModal = new window.bootstrap.Modal(
      document.getElementById("myModal"),
      { backdrop: "static", keyboard: false }
    );
    this.form = this.fb.group({
      adminId:[this.adminDetails._id],
      costType: ["", [Validators.required]],
      costRate: [null, [Validators.required]],
    });

  }

  schoolFormController(){
    this.schoolForm = this.fb.group({
      adminId:[""],
      schoolName: ["", [Validators.required]],

    });
  }
  openDialog(data) {
    this.aboutSchoolDialogBox=false
    this.currentCost = data;
    this.form.patchValue({ ...data });
    this.editValue = true;
    // this.costValue=data.costRate
    // this.fieldName=field
    this.formModal.show();
  }
  openSchoolDialog(data) {
    this.currentSchool = data;
    this.schoolForm.patchValue({ ...data });
    this.schoolEditValue = true;
    this.aboutSchoolDialogBox=true

    // this.costValue=data.costRate
    // this.fieldName=field
    this.formModal.show();
  }
  updateCost() {
    this.aboutSchoolDialogBox=false
    this.formModal.hide();
    const data = { ...this.currentCost, costRate: this.form.value?.costRate };
    this.adminService.UpdateCost(data).subscribe((res) => {
      if (res) {
        Swal.fire({
          icon: "success",
          title: "Congratulation ...",
          text: "Cost Updated Successfully",
          timer: 3000,
        });
        this.form.reset();
        this.getCostData();
      }
    });
  }

  updateSchool() {
    this.aboutSchoolDialogBox=false
    this.formModal.hide();
    const data = { ...this.currentSchool, schoolName: this.schoolForm.value?.schoolName };
    this.adminService.UpdateSchool(data).subscribe((res) => {
      if (res) {
        Swal.fire({
          icon: "success",
          title: "Congratulation ...",
          text: "Cost Updated Successfully",
          timer: 3000,
        });
        this.schoolForm.reset();
        this.getSchoolsData();
      }
    });
  }

  deleteSchool(data) {
    this.adminService.deleteSchool(data).subscribe((res) => {
      if (res) {
        Swal.fire({
          icon: "success",
          title: "Congratulation ...",
          text: "Subject Deleted Successfully",
          timer: 3000,
        });
        this.getSchoolsData();
      }
    });
  }

  addNewCost() {
    this.aboutSchoolDialogBox=false
    this.form.reset();
    this.editValue = false;
    this.formModal.show();
  }
  addNewSchool() {
    this.aboutSchoolDialogBox=true
    this.schoolForm.reset();
    this.schoolEditValue = false;
    this.formModal.show();
  }
  hasError(fieldName, validationName) {
    return (
      this.form.get(fieldName)?.touched &&
      this.form.get(fieldName)?.hasError(validationName)
    );
  }
  hasSchoolError(fieldName, validationName) {
    return (
      this.schoolForm.get(fieldName)?.touched &&
      this.schoolForm.get(fieldName)?.hasError(validationName)
    );
  }
  addCost() {
    this.form.patchValue({adminId:( this.adminDetails._id)})
    this.formModal.hide();
    this.adminService.addCost(this.form.value).subscribe((result) => {
      if (result) {
        Swal.fire({
          icon: "success",
          title: "Congratulation ...",
          text: "Cost Added Successfully",
          timer: 3000,
        });
        this.getCostData();
      }
    });
  }
  addSchool(){
    this.formModal.hide();
    this.schoolForm.patchValue({adminId:( this.adminDetails._id)})
    this.adminService.addSchool(this.schoolForm.value).subscribe((result) => {
      if (result) {
        Swal.fire({
          icon: "success",
          title: "Congratulation ...",
          text: "School Added Successfully",
          timer: 3000,
        });
        this.getSchoolsData()
      }
    });
  }
  getCostData() {
    this.adminService.getCost(this.adminDetails._id).subscribe(
      (result: any) => {
        this.CostRates = result.data[0];
        this.dataSource = new MatTableDataSource(result.data);
      },
      (error) => {
        console.log("error", error.message);
      }
    );
  }

  conformDeleteSchool(data){
    const dialogRef = this.dialog.open(StatusUpdateComponent, {
      data: { dataFor: "deleteSchool" },
      disableClose: true,
      width: "70vw",
      maxWidth: "550px",
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == "yes") {
this.deleteSchool(data)
      }
    })
  }
  getSchoolsData() {
    this.adminService.getSchools(this.adminDetails._id).subscribe(
      (result: any) => {

        this.schoolsDataSource = new MatTableDataSource(result.data);
      },
      (error) => {
        console.log("error", error.message);
      }
    );
  }
}
