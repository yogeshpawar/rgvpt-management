import { ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import * as moment from "moment";
import { Subject, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { END, START } from "../../mockData";
import { CandidateService } from "../../services/candidate.service";
import { ClientService } from "../../services/client.service";
import { StatusUpdateComponent } from "../status-update/status-update.component";
declare var window: any;

@Component({
  selector: "app-client-details",
  templateUrl: "./client-details.component.html",
  styleUrls: ["./client-details.component.scss"],
})
export class ClientDetailsComponent implements OnInit, OnDestroy {
  studentList: any[] = [];
  clientsList: any[] = [];
  subscription: Subscription[] = [];
  private debounceSubject = new Subject<string>();
  moment: any = moment;
  page: number = 1;
  totalClients: number;
  searchValue=""
  start = START;
  end = END;
  statusFilter = "all";
  today = new Date().toLocaleDateString();
  currentCustomer: any;
  checkedBtn = [];
  constructor(
    public cdrf: ChangeDetectorRef,
    public dialog: MatDialog,
    private candidateService: CandidateService,
    private router: Router,
    private clientService: ClientService
  ) {
    // this.getAllClients();
    this.getClients();
    if (this.router.getCurrentNavigation()?.extras?.state?.filter) {
      this.statusFilter =
        this.router.getCurrentNavigation()?.extras?.state?.filter;
      this.selectedStatus(this.statusFilter);
    }
  }


  ngOnInit(): void {
  }
  changeStatus(event, client) {
    const dialogRef = this.dialog.open(StatusUpdateComponent, {
      data: { checked: event.checked, dataFor: "updateStatus" },
      disableClose: true,
      width: "70vw",
      maxWidth: "550px",
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == "yes") {

        this.clientService
          .updateClientData(client._id, {
            ...client,
            profileStatus: event.checked ? "archive" : "unArchive",
          })
          .subscribe((res) => {
            if (res) {
              this.getClients();
              Swal.fire({
                icon: "success",
                title: "Congratulation ...",
                text: "Profile Status Updated Successfully",
                timer: 3000,
              });
            }
          });
      } else {
        event.source.checked = this.checkedBtn[client._id];
      }
    });
  }

  selectedStatus(status) {
    this.page = 1;
    //  this.getAllClients()
    this.getClients();
  }

  renderPage(event: number) {
    this.page = event;
    // this.getAllClients();
    this.getClients();
  }
  getSearchValue(value){
    this.page = 1;
    if(value!="" && value!=null){
      this.searchValue=value
      this.debounceSubject.next(value);
      this.debounceSubject.pipe(
        debounceTime(2000),
        distinctUntilChanged(),
      ).subscribe((value) => {
        this.getClients()
      });
    }else {
      this.searchValue=''
      this.getClients()
    }
  }
  getClients() {
    const status = this.statusFilter == "all" ? "undefined" : this.statusFilter;
    this.clientService.getAllClients(status, this.page,this.searchValue).subscribe(
      (result: any) => {
        if (result) {
          console.log("data all client", result);
          this.totalClients = result?.total ?? 0;
          this.clientsList = result?.data;

          for (let client of this.clientsList) {
            this.checkedBtn[client._id] =
              client?.profileStatus == "archive" ? true : false;
          }
        }
      },
      (error) => {
        console.log("error", error);
        Swal.fire({
          icon: "error",
          title: `Oops... `,
          text: "Internal Server Error",
          timer: 3000,
        });
      }
    );
  }


  viewChildrens(data) {
    const dialogRef = this.dialog.open(StatusUpdateComponent, {
      data: { ...data, dataFor: "displayChildrens" },
      disableClose: true,
      width: "85vw",
      maxWidth: "900px",
    });
  }

  viewClientProfile(data, action) {
    this.router.navigate(["add-client"], {
      state: { currentUser: data, action: action },
    });
  }

  ngOnDestroy(): void {
    this.subscription.forEach((s) => s.unsubscribe());
  }
}
