import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from "@angular/common/http";
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { AuthService } from "../services/auth.service";
@Injectable()
export class HeadersInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const req = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`,
      },
    });
    return next.handle(req).pipe(
      //if token expired then logged out the user and redirect to login page
      catchError((err) => {
        console.log("intercept",err)

        if (err instanceof HttpErrorResponse) {
          if (err.status === 401 && err.statusText == "Unauthorized") {
            console.log("interceptor",err);
            this.auth.logOutAdmin();
          }
        }
        if(err.error.message){
          Swal.fire({
            icon: "error",
            title: `Oops...${err?.status} `,
            text: err?.error?.message,
            timer: 3000,
          });
        }
        // Swal.fire({
        //   icon: 'error',
        //   title: 'Oops...',
        //   text: err?.statusText,
        //   timer:3000
        // })
        return Observable.throw(err);
      })
    );
  }
}
