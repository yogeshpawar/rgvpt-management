import { Directive, HostListener } from '@angular/core';
@Directive({
  selector: '[appAlphanumeric]'
})
export class AlphanumericDirective {

  constructor() { }
  @HostListener('keydown', ['$event']) public onKeydown(event: KeyboardEvent) {
    const regEx = new RegExp(/^[a-zA-Z0-9]*$/);
    if (!regEx.test(event.key)) {
      event.preventDefault();
    }
  }
}
