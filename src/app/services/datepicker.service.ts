import { Injectable } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { DateRange, MatDateRangeSelectionStrategy } from '@angular/material/datepicker';

@Injectable()
export class DatepickerService
  implements MatDateRangeSelectionStrategy<string>
{
  constructor(private _dateAdapter: DateAdapter<string>) {}

  selectionFinished(date: string | null): DateRange<string> {
    return this._createFiveDayRange(date);
  }

  createPreview(activeDate: string | null): DateRange<string> {
    return this._createFiveDayRange(activeDate);
  }

  private _createFiveDayRange(date: string | null): DateRange<any> {
    if (date) {
      const d = new Date(date);
      const day = d.getDay();
      const diff = d.getDate() - day + (day == 0 ? -4 : 1);
      if(diff<0){
        return new DateRange<string>(null, null) ;
      }
      const start = new Date(d.setDate(diff));
      const end = new Date(d.setDate(diff + 4));
      return new DateRange<any>(start, end);
    }
    return new DateRange<string>(null, null);
  }
}
