import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import * as myGlobals from "../globalPath";
import { END, START } from "../mockData";
import { Children, Client } from "../model";
import { AdminService } from "./admin.service";
@Injectable({
  providedIn: "root",
})
export class ClientService {
  constructor(private _http: HttpClient,private adminService:AdminService) {
    this.getAdminDetails();
  }
  base_Url=myGlobals.baseUrl;
adminId:any

  getChildrens() {
    if(this.adminId){
      // return this._http.get<Children>(`${this.base_Url}client/get-childrens/${this.adminId}`);
      return this._http.get<Children>(this.base_Url + 'client/get-childrens/'+ this.adminId);


    }
  }
  postClient(data: Client) {
    return this._http.post<Client>(`${this.base_Url}client/postClient`, data);
  }
  getClients(fromDate, toDate, status?, page?,search?) {
    if(this.adminId){
      return this._http.get<Client>(
        `${this.base_Url}client/getClient/${this.adminId}?status=${
          status ? status : "undefined"
        }&page=${page}&fromDate=${fromDate}&toDate=${toDate}&search=${search}`
      );
    }

  }
  getAllClients(profileStatus?,page?,searchValue?){
    if(this.adminId){
      return this._http.get<Client>(
        `${this.base_Url}client/allClients/${this.adminId}?page=${page}&profileStatus=${profileStatus}&search=${searchValue}`
      );
    }
  }
  updateClientData(id, data) {
    console.log("update service",id,data)
    return this._http.put<Client>(
      `${this.base_Url}client/update-client/${id}`,
      data
    );
  }

  getClientsCount() {
    if(this.adminId){
      console.log("admin id",this.adminId)
      return this._http.get(`${this.base_Url}client/clientsCount/${this.adminId}?fromDate=${START}&toDate=${END}`);

    }
  }
  getLatestInvoiceWeek(id){
return this._http.get(`${this.base_Url}client/invoice/${id}`)
  }
  updateClientStatus(client: Client, data) {
    return this._http.put<Client>(
      `${this.base_Url}client/update-client-status/${client._id}`,
      data
    );
  }
  getWeeklyPayment(id) {
    return this._http.get(`${this.base_Url}client/get-weeklyPayment/${id}`);
  }
  exportClient(fromDate, toDate, status?,search?): Observable<any> {

      return this._http
      .get(
        `${this.base_Url}export/exportClients/${this.adminId}?status=${
          status != "all" ? status : "undefined"
        }&page=undefined&fromDate=${fromDate}&toDate=${toDate}&search=${search}`,
        { responseType: "blob" }
      )
      .pipe(
        map((data) => {
          if (data) {
            this.downloadFile(data, fromDate, toDate);
          }
          console.log("data", data);
          return data;
        })
      );


  }

  downloadFile(blob, fromDate, toDate) {
    console.log("downloadFile");
    const aElement = document.createElement("a");
    const objectUrl = window.URL.createObjectURL(blob);
    aElement.href = objectUrl;
    aElement.download = "client" + "-" + fromDate + "---" + toDate + ".xlsx";
    aElement.click();
    window.URL.revokeObjectURL(objectUrl);
  }

  getAdminDetails() {
    this.adminId=JSON.parse(localStorage.getItem("userDetails"))._id
  }



}
