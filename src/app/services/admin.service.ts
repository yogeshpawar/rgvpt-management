import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as myGlobals from "../globalPath";

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  base_Url=myGlobals.baseUrl;


  constructor(private _http:HttpClient) { }

  getCost(adminId:any){
    return this._http.get(`${this.base_Url}admin/getCost/${adminId}`)
  }
  getSchools(adminId:any){
    return this._http.get(`${this.base_Url}admin/getSchools/${adminId}`)
  }
  UpdateCost(data){
    return this._http.put(`${this.base_Url}admin/updateCost/${data._id}`,data)
  }
  UpdateSchool(data){
    return this._http.put(`${this.base_Url}admin/updateSchool/${data._id}`,data)
  }
  addCost(data){
    return this._http.post(`${this.base_Url}admin/postCost`,data)
  }
  addSchool(data){
    return this._http.post(`${this.base_Url}admin/postSchool`,data)
  }
  deleteSchool(data){
    return this._http.delete(`${this.base_Url}admin/deleteSchool/${data._id}`,data)
  }
}
