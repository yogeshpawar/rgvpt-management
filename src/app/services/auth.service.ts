import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as myGlobals from "../globalPath";
@Injectable({
  providedIn: 'root'
})
export class AuthService {
base_Url=myGlobals.baseUrl;
  constructor(private _http:HttpClient) { }

  adminLogin(data){
  return this._http.post(this.base_Url + 'auth/login',data)
  }

  verifyEmailAddress(data){
    return this._http.post(this.base_Url +'auth/emailCheck',data)
  }

  resetAdminPassword(data){
    return this._http.put(this.base_Url +'auth/updatePassword',data)
  }
getToken(){
  return localStorage.getItem('token');
}

logOutAdmin(){
  console.log("admin log out")
  localStorage.removeItem('token');
  localStorage.removeItem('userDetails');
  localStorage.removeItem('user');
  window.location.href = "/login"
}

}
