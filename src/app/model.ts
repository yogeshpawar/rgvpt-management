
export interface LOGIN{
  email:string,
  password:string
}
export interface Children{
  _id?:string
  childName: string,
  dateOfBirth: string,
  schoolName: string,
  costType: string,
  payableAmount:number,
clientId?:string
}

export interface weeklyPayment{
  _id:string,
fromDate:string
toDate:string
paymentStatus:string
paidAmount:number
}
export interface CostRates{
  returnRate:number,
midRate:number,
fullRate:number
}
export interface Client{
  _id?:string
  clientId: string,
  fullName:string,
  primaryPhoneNumber:string,
   secondaryPhoneNumber: string ,
  totalAmount: number,
  relationship: string,
  childrens?: [Children],
  weeklyPayment?:[weeklyPayment] |{weeklyPayment}

}
