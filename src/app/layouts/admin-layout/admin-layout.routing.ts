import { Routes } from '@angular/router';
import { AddClientComponent } from '../../components/add-client/add-client.component';
import { BillingDetailComponent } from '../../components/billing-detail/billing-detail.component';
import { ClientDetailsComponent } from '../../components/client-details/client-details.component';
import { PaymentCollectComponent } from '../../components/payment-collect/payment-collect.component';
import { SettingsComponent } from '../../components/settings/settings.component';
import { DashboardComponent } from '../../dashboard/dashboard.component';
export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'billing-detail',component: BillingDetailComponent },
    // {path: 'statusUpdate',     component:StatusUpdateComponent},
    {path:'payment',component:PaymentCollectComponent},
    {path:'settings',          component:SettingsComponent},
    { path:'client-details', component:ClientDetailsComponent},
    { path: 'add-client',      component: AddClientComponent },
];
